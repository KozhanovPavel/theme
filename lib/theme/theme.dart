import 'package:flutter/material.dart';
import 'package:theme/theme/app_colors.dart';
import 'package:theme/theme/app_text.dart';



final customeTheme = ThemeData(
  // // primaryColor: AppColors.primaryColor,
  // accentColor: AppColors.ChartPinkColor,


  // Тема для иконок
  iconTheme: IconThemeData(color: AppColors.primaryColor),

  // Тема для AppBar
  appBarTheme: AppBarTheme(
    color: AppColors.primaryColor,
    titleTextStyle: AppTextStyle.headline3,
    elevation: 0,
    centerTitle: true,
  ),

  // Тема для тестового поля
  inputDecorationTheme: InputDecorationTheme(
    hintStyle: AppTextStyle.headline5,
    contentPadding: EdgeInsets.only(top: 0, bottom: -20, left: 5, right: 0),
    border: UnderlineInputBorder(
        borderSide: BorderSide(color: AppColors.greyColor)),
  ),

  // Тема для кнопки OutlinedButton
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: ButtonStyle(
      overlayColor: MaterialStateProperty.all(AppColors.ChartPinkColor),
      minimumSize: MaterialStateProperty.all(Size(130, 50)),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: BorderSide(
            width: 1,
            color: AppColors.primaryColor,
            style: BorderStyle.solid,
          ),
        ),
      ),
      backgroundColor: MaterialStateProperty.all(Colors.white),
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: BorderSide(
            width: 1,
            color: AppColors.greyColor,
            style: BorderStyle.solid,
          ),
        ),
      ),
      shadowColor: MaterialStateProperty.all(Colors.white),
      overlayColor: MaterialStateProperty.all(AppColors.ChartPinkColor),
      padding: MaterialStateProperty.all(EdgeInsets.zero),
      minimumSize: MaterialStateProperty.all(Size(60, 30)),
      textStyle: MaterialStateProperty.all(AppTextStyle.headline1),
      backgroundColor: MaterialStateProperty.all(Colors.white),
    ),
  ),
);
