import 'package:flutter/material.dart';
import 'package:theme/theme/app_colors.dart';

// Шрифты
class AppTextFonts {
  AppTextFonts._();

  static const String fnDelaGothicOne = 'DelaGothicOne';
  static const String fnRubik = 'Rubik';
  static const String fnRubikLight = 'RubikLight';
}

// Стили текста
class AppTextStyle {
  AppTextStyle._();

  static final TextTheme lightTextTheme = TextTheme(
    headline1: headline1,
    headline2: headline2,
    headline3: headline3,
    headline4: headline4,
    headline5: headline5,
    headline6: headline6,
    bodyText1: headline7,
    bodyText2: headline8,
    caption: headline9,
  );

  static final TextStyle headline1 = TextStyle(
    fontSize: 30,
    color: AppColors.primaryColor,
    fontFamily: AppTextFonts.fnDelaGothicOne,
  );
  static final TextStyle headline2 = TextStyle(
    fontSize: 20,
    color: AppColors.blackColor,
    fontFamily: AppTextFonts.fnRubik,
  );

  static final TextStyle headline3 = TextStyle(
    fontSize: 18,
    color: AppColors.greyColor,
    fontFamily: AppTextFonts.fnRubik,
  );
  static final TextStyle headline4 = TextStyle(
    fontSize: 18,
    color: AppColors.blackColor,
    fontFamily: AppTextFonts.fnRubik,
  );

  static final TextStyle headline5 = TextStyle(
      fontSize: 18,
      color: AppColors.greyColor,
      fontFamily: AppTextFonts.fnRubikLight,
      fontWeight: FontWeight.w300);
  static final TextStyle headline6 = TextStyle(
      fontSize: 18,
      color: AppColors.blackColor,
      fontFamily: AppTextFonts.fnRubikLight,
      fontWeight: FontWeight.w300);
  static final TextStyle headline7 = TextStyle(
    fontSize: 16,
    color: AppColors.blackColor,
    fontFamily: AppTextFonts.fnRubik,
  );
  static final TextStyle headline8 = TextStyle(
      fontSize: 16,
      color: AppColors.blackColor,
      fontFamily: AppTextFonts.fnRubikLight,
      fontWeight: FontWeight.w300);
  static final TextStyle headline9 = TextStyle(
    fontSize: 14,
    color: AppColors.greyColor,
    fontFamily: AppTextFonts.fnRubik,
  );
}
