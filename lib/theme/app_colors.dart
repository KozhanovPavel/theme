import 'package:flutter/material.dart';

// Цвета используемые в приложении
class AppColors {
  AppColors._();
  static const Color primaryColor = Color(0xFF961E73);
  static const Color blackColor = Color(0xFF000000);
  static const Color greyColor = Color(0xFF969696);
  static const Color greenColor = Color(0xFF00C82D);

  static const Color ChartPinkColor = Color(0xFFF5E9F1);
  static const Color TextPinkColor = Color(0xFFF5E9F1);
  static const Color ChartGreenColor = Color(0xFFE5F9EA);
  static const Color TextBubbleGreyColor = Color(0xFFEAEAEA);
}
