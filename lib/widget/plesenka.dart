import 'package:flutter/material.dart';
import 'package:theme/theme/app_colors.dart';
import 'package:theme/theme/app_text.dart';

class Plesenka extends StatelessWidget {
  final String? image;
  final bool triger;
  const Plesenka({
    Key? key,
    this.image,
    required this.triger,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: collect(triger, image),
    );
  }
}

Widget card(image, color, {blurRadius, spreadRadius}) {
  return Container(
    height: 130,
    width: 85,
    decoration: BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: color,
          blurRadius: blurRadius ?? 6,
          spreadRadius: spreadRadius ?? 0,
        ),
      ],
      borderRadius: BorderRadius.circular(10),
    ),
    child: Center(
      child: imageOrNot(image: image),
    ),
  );
}

Widget collect(triger, image) {
  if (triger == true && image != null) {
    const Color color = AppColors.greenColor;
    return card(image, color);
  } else if (triger == false) {
    const double blurRadius = 0;
    const double spreadRadius = 0;
    const Color color = AppColors.greyColor;
    return card(
      image,
      color,
      blurRadius: blurRadius,
      spreadRadius: spreadRadius,
    );
  } else {
    const Color color = AppColors.greyColor;
    return card(
      image,
      color,
    );
  }
}

Widget imageOrNot({String? image}) {
  if (image != null) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image:
            DecorationImage(fit: BoxFit.fill, image: Image.asset(image).image),
      ),
    );
  } else {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 25, top: 35),
          child: Column(
            children: [
              Text(
                'Добавить',
                style: AppTextStyle.headline3.copyWith(fontSize: 14),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                'Плесеньку',
                style: AppTextStyle.headline3.copyWith(fontSize: 14),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 5),
          alignment: Alignment.bottomRight,
          child: Column(
            children: [
              Icon(
                Icons.control_point,
                color: AppColors.greyColor,
              ),
            ],
          ),
        )
      ],
    );
  }
}
