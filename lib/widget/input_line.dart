import 'package:flutter/material.dart';
import 'package:theme/theme/app_text.dart';
import 'package:theme/theme/theme.dart';

class InputLine extends StatefulWidget {
  InputLine({Key? key, required this.hintText}) : super(key: key);
  final String hintText;

  @override
  _InputLineState createState() => _InputLineState(hintText);
}

class _InputLineState extends State<InputLine> {
  final String hintText;

  _InputLineState(this.hintText);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 0),
        child: TextFormField(
          decoration: InputDecoration(
            hintText: hintText,
            focusedBorder: UnderlineInputBorder(
                borderSide:
                    customeTheme.inputDecorationTheme.border!.borderSide),
          ),
          style: AppTextStyle.headline4,
          validator: (value) {
            if (value!.isEmpty) {
              return 'Вы ничего не ввели';
            }
            return null;
          },
        ),
      ),
    );
  }
}
