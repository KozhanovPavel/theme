import 'package:flutter/material.dart';
import 'package:theme/theme/app_text.dart';
import 'package:theme/theme/theme.dart';

class LittleButton extends StatelessWidget {
  final Icon icon;
  final int count;
  final Function onPress;
  const LittleButton({
    Key? key,
    required this.icon,
    required this.count,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0),
      child: Container(
        width: 75,
        height: 35,
        child: ElevatedButtonTheme(
          data: customeTheme.elevatedButtonTheme,
          child: ElevatedButton(
            onPressed: () {},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '$count',
                  style: AppTextStyle.headline2.copyWith(fontSize: 16),
                ),
                SizedBox(
                  width: 4,
                ),
                IconTheme(
                    data: customeTheme.iconTheme.copyWith(size: 24),
                    child: icon),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
