import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:theme/theme/app_text.dart';

class Comment extends StatelessWidget {
  const Comment({
    Key? key,
    required this.firstName,
    required this.lastName,
    required this.comment,
    required this.dateTime,
    required this.image,
  }) : super(key: key);
  final String firstName;
  final String lastName;
  final String comment;
  final String dateTime;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(60), color: Colors.red),
              child: CircleAvatar(
                backgroundImage: Image.asset(image).image,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.75,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$firstName $lastName',
                    style: AppTextStyle.headline7,
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    '$comment',
                    style: AppTextStyle.headline8,
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    '$dateTime',
                    style: AppTextStyle.headline9,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
