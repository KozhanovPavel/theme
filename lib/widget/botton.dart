import 'package:flutter/material.dart';
import 'package:theme/theme/app_colors.dart';
import 'package:theme/theme/app_text.dart';
import 'package:theme/theme/theme.dart';

class CastomBotton extends StatelessWidget {
  final onTap;
  final Icon? icon;
  final String text;
  final bool triger;
  CastomBotton({
    required this.onTap,
    required this.text,
    this.icon,
    required this.triger,
  });

  @override
  Widget build(BuildContext context) {
    // Основной контейнер
    return Container(
      width: 200,
      height: 60,

      // Вызов готовой кнопки
      child: bottonStyle(triger, icon, text, onTap),
    );
  }
}

// Виджет кнопки
Widget botton(icon, text, Color colors, stroke, blur, blurRad, onTap) {
  // Контейнер дающий тень
  return Container(
    decoration: BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: colors,
          spreadRadius: blurRad,
          blurRadius: blur,
        )
      ],
      borderRadius: BorderRadius.circular(15),
    ),

    // Тема кнопки
    child: OutlinedButtonTheme(
      data: OutlinedButtonThemeData(
        style: customeTheme.outlinedButtonTheme.style!.copyWith(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: BorderSide(
                color: colors,
                width: stroke,
              ),
            ),
          ),
        ),
      ),

      // Кнопка
      child: OutlinedButton(
        onPressed: onTap,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: textWhithICon(icon, colors, text),
        ),
      ),
    ),
  );
}

// Виджет собирющий кнопку, деалает кнопку серой или primaryColor
Widget bottonStyle(triger, icon, text, onTap) {
  if (triger == true) {
    Color colors = AppColors.primaryColor;
    double stroke = 0;
    double blur = 3;
    double blurRad = 0;
    return botton(
      icon,
      text,
      colors,
      stroke,
      blur,
      blurRad,
      onTap,
    );
  } else {
    Color colors = AppColors.greyColor;
    double stroke = 1;
    double blur = 4;
    double blurRad = -3;
    return botton(
      icon,
      text,
      colors,
      stroke,
      blur,
      blurRad,
      onTap,
    );
  }
}

// Метода для вывода с иконкой или без
List<Widget> textWhithICon(icon, colors, text) {
  if (icon != null) {
    return [
      Text(
        text,
        style: AppTextStyle.headline2.copyWith(color: colors),
      ),
      SizedBox(
        width: 5,
      ),
      IconTheme(
        data: customeTheme.iconTheme.copyWith(color: colors),
        child: icon,
      ),
    ];
  } else {
    return [
      Text(
        text,
        style: AppTextStyle.headline2.copyWith(color: colors),
      ),
    ];
  }
}
