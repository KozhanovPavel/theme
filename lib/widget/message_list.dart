import 'package:flutter/material.dart';
import 'package:theme/theme/app_text.dart';

class MessageList extends StatelessWidget {
  const MessageList(
      {Key? key,
      required this.firstName,
      required this.lastName,
      required this.message,
      required this.dateTime,
      required this.image})
      : super(key: key);
  final String firstName;
  final String lastName;
  final String message;
  final String dateTime;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(200),
            ),
            child: CircleAvatar(
              backgroundImage: Image.asset('assets/image/foto.jpg').image,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.75,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '$firstName $lastName',
                      style: AppTextStyle.headline7,
                    ),
                    Text(dateTime, style: AppTextStyle.headline9),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Text(message,
                    style: AppTextStyle.headline8),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
