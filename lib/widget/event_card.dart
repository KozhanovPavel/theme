import 'package:flutter/material.dart';
import 'package:theme/theme/app_colors.dart';
import 'package:theme/theme/app_text.dart';

class EventCard extends StatelessWidget {
  final String? image;
  final String date;
  final String time;
  final String name;
  const EventCard({
    Key? key,
    this.image,
    required this.date,
    required this.time,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            color: AppColors.greyColor,
            blurRadius: 8,
            spreadRadius: -1,
          ),
        ],
      ),
      child: collect(date, time, name, image),
    );
  }
}

Widget card(date, time, name, {String? image}) {
  return Container(
    height: 260,
    width: 365,
    child: Center(
      child: Column(
        children: [
          Expanded(
            flex: 5,
            child: Container(
              child: imageOrNot(image: image),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                ),
                boxShadow: [
                  BoxShadow(
                      color: AppColors.greyColor,
                      blurRadius: 8,
                      spreadRadius: -1,
                      offset: Offset(1, -1)),
                ],
              ),
              child: Container(
                  padding: EdgeInsets.only(
                    top: 10,
                    left: 10,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                    ),
                  ),
                  child: Row(
                    children: [
                      Column(
                        children: [
                          Text(
                            '$date',
                            style:
                                AppTextStyle.headline3.copyWith(fontSize: 18),
                          ),
                          Text(
                            '$time',
                            style: AppTextStyle.headline2.copyWith(
                              fontSize: 30,
                              color: AppColors.greyColor,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        children: [
                          Container(
                            width: 180,
                            child: Text(
                              '$name',
                              style:
                                  AppTextStyle.headline2.copyWith(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget collect(date, time, name, image) {
  if (image != null) {
    return card(date, time, name, image: image);
  } else {
    return card(date, time, name);
  }
}

Widget imageOrNot({String? image}) {
  if (image != null) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
        image:
            DecorationImage(fit: BoxFit.fill, image: Image.asset(image).image),
      ),
    );
  } else {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 25, top: 35),
          child: Column(
            children: [
              Text(
                'Добавить',
                style: AppTextStyle.headline3.copyWith(fontSize: 22),
              ),
              SizedBox(
                height: 2,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Обложку события',
                    style: AppTextStyle.headline3.copyWith(fontSize: 22),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Icon(
                    Icons.control_point,
                    color: AppColors.greyColor,
                    size: 28,
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
