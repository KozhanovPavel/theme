import 'package:flutter/material.dart';
import 'package:theme/theme/app_colors.dart';
import 'package:theme/theme/app_text.dart';

class Place extends StatelessWidget {
  const Place({
    Key? key,
    required this.image,
    required this.name,
    required this.address,
    required this.geo,
  }) : super(key: key);
  final String image;
  final String name;
  final String address;
  final String geo;

  @override
  Widget build(BuildContext context) {
    const Icon icon = Icon(
      Icons.location_on,
      size: 18,
      color: AppColors.greyColor,
    );
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 75,
              height: 80,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: Image.asset(image).image,
                    fit: BoxFit.cover,
                  )),
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.75,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(name, style: AppTextStyle.headline2),
                  Text(address, style: AppTextStyle.headline3),
                  Row(
                    children: [
                      icon,
                      Text('$geo', style: AppTextStyle.headline3),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
