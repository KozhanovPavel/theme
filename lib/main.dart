import 'package:flutter/material.dart';
import 'package:theme/theme/app_text.dart';
import 'package:theme/theme/theme.dart';
import 'package:theme/widget/botton.dart';
import 'package:theme/widget/comment.dart';
import 'package:theme/widget/event_card.dart';
import 'package:theme/widget/input_line.dart';
import 'package:theme/widget/little_botton.dart';
import 'package:theme/widget/message_list.dart';
import 'package:theme/widget/place.dart';
import 'package:theme/widget/plesenka.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: customeTheme,
      home: MyHomePage(title: 'Theme'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool triger = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Theme',
          style: AppTextStyle.headline1.copyWith(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              text('Название', AppTextStyle.headline1),
              SizedBox(height: 10),
              text('Заголовок', AppTextStyle.headline2),
              SizedBox(height: 10),
              text('Подзаголовок', AppTextStyle.headline3),
              SizedBox(height: 10),
              text('Поля ввода', AppTextStyle.headline4),
              SizedBox(height: 10),
              text('Поля ввода (Текст по умолчанию)', AppTextStyle.headline5),
              SizedBox(height: 10),
              text('Текст статьи', AppTextStyle.headline6),
              SizedBox(height: 10),
              text('Комментарии/Сообщания (имя)', AppTextStyle.headline7),
              SizedBox(height: 10),
              text('Комментарии/Сообщания (Текст)', AppTextStyle.headline8),
              SizedBox(height: 10),
              text('Комментарии/Сообщания (дата)', AppTextStyle.headline9),
              SizedBox(height: 10),

              Comment(
                firstName: 'Вадим',
                lastName: 'Жигулев',
                comment:
                    'Безусловно это лучший автохтон и редкий терруар! d;fs;df; sdf sdlf sdfl sdlf awfkawkfkw fk',
                dateTime: 'Вчера',
                image: 'assets/image/foto.jpg',
              ),
              SizedBox(height: 20),

              Comment(
                firstName: 'Евгений',
                lastName: 'Груль',
                comment:
                    'фцаф фадфцд фцда дфцад фцащдкырдк рк ыу ыуаь а лтуыт щыут аыута зщыутащз туыз ыута зутазщыут',
                dateTime: 'Вчера',
                image: 'assets/image/foto.jpg',
              ),
              // InputText(),
              SizedBox(height: 20),

              MessageList(
                message: 'Спасибо за встречу? Было очень позновательно',
                firstName: 'Сергей',
                lastName: 'Вараздатович',
                dateTime: '07.12.20',
                image: 'assets/image/foto.jpg',
              ),
              SizedBox(height: 20),

              Place(
                image: 'assets/image/bar.jpg',
                address: 'Б. Никитская 12',
                name: 'Сквот Винная',
                geo: '1.7 км',
              ),
              SizedBox(height: 20),

              InputLine(
                hintText: 'Расскажите о событии',
              ),
              SizedBox(height: 20),

              CastomBotton(
                onTap: () {
                  setState(() {
                    if (triger == false)
                      triger = true;
                    else
                      triger = false;
                  });
                },
                text: 'Пригласить',
                icon: Icon(Icons.check),
                triger: triger,
              ),
              SizedBox(height: 20),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  LittleButton(
                    icon: Icon(Icons.favorite_border),
                    count: 103,
                    onPress: () {},
                  ),
                  SizedBox(width: 4),
                  LittleButton(
                    icon: Icon(Icons.chat_bubble_outline),
                    count: 17,
                    onPress: () {},
                  ),
                  SizedBox(width: 4),
                  LittleButton(
                    icon: Icon(Icons.check),
                    count: 24,
                    onPress: () {},
                  ),
                ],
              ),

              Container(
                height: 200,
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 5),
                      Plesenka(
                        triger: true,
                      ),
                      SizedBox(width: 13),
                      Plesenka(
                        triger: false,
                        image: 'assets/image/bar.jpg',
                      ),
                      SizedBox(width: 13),
                      Plesenka(
                        triger: true,
                        image: 'assets/image/bar.jpg',
                      ),
                      SizedBox(width: 13),
                      Plesenka(
                        triger: true,
                        image: 'assets/image/bar.jpg',
                      ),
                      SizedBox(width: 13),
                      Plesenka(
                        triger: true,
                        image: 'assets/image/bar.jpg',
                      ),
                      SizedBox(width: 13),
                      Plesenka(
                        triger: true,
                        image: 'assets/image/bar.jpg',
                      ),
                      SizedBox(width: 13),
                      Plesenka(
                        triger: true,
                        image: 'assets/image/bar.jpg',
                      ),
                      SizedBox(width: 13),
                      Plesenka(
                        triger: true,
                        image: 'assets/image/bar.jpg',
                      ),
                      SizedBox(width: 13),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              EventCard(
                date: 'Пт, 01 Сен',
                name: 'Сходка сомелье в "Хлеб и Вино"',
                time: '18:00',
                // image: 'assets/image/bar.jpg',
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}

Widget text(String text, TextStyle style) {
  return Text(text, style: style);
}
